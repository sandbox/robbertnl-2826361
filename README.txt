This module uses an REGEX to extract all files embedded in the WYSIWYG editor and tries to map it to a managed Drupal file.
The managed Drupal file is stored in a seperate field (field_inline_attachments). This makes indexing
attachments in WYSIWYG areas possible.

How to use:
- Add a multiple file field with system name 'field_inline_attachments' to all content types that should be included
- Add this field to the contenttypes search index display and set it to hidden in other displays. (admin/structure/types/manage/<content type name>/display/
- Configure the additional settings provided by this module: See the box 'Inline attachments' at admin/config/search/apachesolr/attachments
- Enable file types for indexing at admin/config/search/apachesolr/settings/solr/index
- Resave all existing content (this will trigger the module)
- Delete & reindex the Apache solr index
